import { StyleSheet } from 'react-native';

export const styles = StyleSheet.create({
    "text": {
        fontSize: 20
    },
    "userImage":{
        width: 60,
        height: 60,
        borderRadius: 100,
        marginLeft: 10,
        marginRight: 10 
    },
    "userText":{
        height: 80,
        flexDirection: "row",
        alignItems: "center",
        borderTopColor: "#000",
        borderStyle: "solid",
        backgroundColor: "white",
        fontWeight: "bold",
        zIndex: 1
    },
    "textInfo":{
        marginLeft: 10,
        marginVertical: 10,
        fontSize: 20
    },
    "todoLayout":{
        flex: 1,
        backgroundColor: "#E5E7E9"
    },
    "nextTasksView":{
        backgroundColor: "white",
        paddingVertical: 20
    },
    "nextTasksCard":{
        height: 130,
        width: 200,
        backgroundColor: "green",
        marginHorizontal: 10,
        borderRadius:30
    },
    "taskCardProgress":{
        height: 250,
        backgroundColor: "blue",
        marginBottom: 10,
        marginHorizontal: 10,
        borderRadius: 30
    }
}); 
