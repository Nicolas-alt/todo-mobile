import React from 'react';
import {Text, View, ScrollView, Image} from 'react-native';

import { styles } from '../assets/styles/homeStyles';

const Home = () => {
    return (
        <>
        <View style={styles.userText}>
            <Image style={styles.userImage} source={require('../assets/img/user.jpg')} />
            <View>
                <Text style={styles.text}>Hello Nicolas!</Text>            
                <Text>Colombia</Text>
            </View>
        </View>
        
        <ScrollView style={styles.todoLayout}>
            {/* your next tasks */}
            <View style={styles.nextTasksView}>
            <Text style={styles.textInfo}>Your next tasks:</Text>
                <ScrollView horizontal  >
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                    <View style={styles.nextTasksCard}>
                        <Text>Ver algo</Text>
                    </View>
                </ScrollView>
            </View>
            
            {/* in progress task */}
            <View style={styles.nextTasksView}>
                <Text style={styles.textInfo}>Tasks in progress:</Text>
                <View>
                    <View style={styles.taskCardProgress}>
                        <Text>Hacer comida</Text>
                    </View>
                    <View style={styles.taskCardProgress}>
                        <Text>Hacer comida</Text>
                    </View>
                    <View style={styles.taskCardProgress}>
                        <Text>Hacer comida</Text>
                    </View>
                    <View style={styles.taskCardProgress}>
                        <Text>Hacer comida</Text>
                    </View>
                    <View style={styles.taskCardProgress}>
                        <Text>Hacer comida</Text>
                    </View>
                </View>
            </View>
        </ScrollView>
        </>
    )
}



export default Home;
